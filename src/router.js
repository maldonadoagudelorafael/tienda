import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import AcercaDe from "./views/AcercaDe"
import Login from "./views/Login";
import Carrito from "./views/Carrito";
Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: AcercaDe
    },
    {
      path: '/login',
      name: 'login',
      component: Login

    },
    {
      path: '/carrito',
      name: 'carrito',
      component:Carrito

    }
  ]
})
